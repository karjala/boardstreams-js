## DESCRIPTION

BoardStreams is an easy way to use WebSockets on the browser and on the server.

Documentation is currently at [boardstreams.dev](https://boardstreams.dev)
