import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import babel from "rollup-plugin-babel";
import pkg from "./package.json";
import {terser} from 'rollup-plugin-terser';

export default [
    {
        input: "src/index.js", // your entry point
        output: {
            name: "BoardStreams", // package name
            file: pkg.browser,
            format: "umd",
            sourcemap: true,
        },
        plugins: [
            resolve(),
            commonjs(),
            babel({
                exclude: ["node_modules/**"],
                runtimeHelpers: true,
            }),
            terser(),
        ],
    },
    {
        input: "src/index.js", // your entry point
        output: [
            // { file: pkg.main, format: "cjs" },
            {
                file: pkg.module,
                format: "es",
                sourcemap: true,
            },
        ],
        plugins: [
            babel({
                exclude: ["node_modules/**"],
                runtimeHelpers: true,
            }),
            terser(),
        ],
    },
];
